var express = require('express');
var router = express.Router();
const soap = require('soap');
const bodyParser = require('body-parser');
var cors = require('cors')


const ADRES = 'https://tckimlik.nvi.gov.tr/service/kpspublic.asmx?WSDL'; // https://tckimlik.nvi.gov.tr/service/kpspublic.asmx?WSDL  , http://SUNUCUIP/UlasimServis/Service.asmx?wsdl

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/test', cors() ,function(req, res, next) {
 // console.log(req.query);
 // res.send({});


  let degerler = {
    TCKimlikNo: req.query.TCKimlikNo,
    Ad: req.query.Ad,
    Soyad: req.query.Soyad,
    DogumYili: req.query.DogumYili
  };


  soap.createClient(ADRES, (err, client) => {

    client.TCKimlikNoDogrula(degerler, (err, result) => { // TODO: method değiştir
      if (result.TCKimlikNoDogrulaResult) {
        res.send({
          'result': true
        });
      } else {
        res.send({
          'result': false
        });
      }
    });

  });
});

module.exports = router;
